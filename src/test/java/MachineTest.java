import org.junit.Assert;
import org.junit.Test;

public class MachineTest {

    @Test
    public void shouldReturn3and0ForInputCheatedAndCooperated() {
        Machine machine = new Machine();
        machine.calculateScore("cheated", "cooperated");

        Assert.assertEquals( 3, machine.playerOneScore );
        Assert.assertEquals( -1, machine.playerTwoScore );
    }

    @Test
    public void shouldReturn0and3ForInputCooperatedAndCheated() {
        Machine machine = new Machine();
        machine.calculateScore("cooperated", "cheated");

        Assert.assertEquals( -1, machine.playerOneScore );
        Assert.assertEquals( 3, machine.playerTwoScore );
    }

    @Test(expected=IllegalArgumentException.class)
    public void shouldHandleInvalidInput() {
        Machine machine = new Machine();
        machine.calculateScore("abcd", "cheated");
    }

}
