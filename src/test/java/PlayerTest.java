import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class PlayerTest {

    @Test
    public void shouldReturnPlayerInitialScore() {
        System.setIn(new BufferedInputStream(new ByteArrayInputStream("0".getBytes())));
        Scanner scanner = new Scanner(System.in);
        Player player = new Player( "Älex" ,scanner);
        int score  = player.getScore();
        Assert.assertEquals( 0, score );
    }

    @Test
    public void shouldMake_Cooperated_Move() {
        System.setIn(new BufferedInputStream(new ByteArrayInputStream("0".getBytes())));
        Scanner scanner = new Scanner(System.in);
        Player player = new Player( "Älex" ,scanner);
        String move = player.askTheMove();
        Assert.assertEquals( "cooperated", move );
    }

    @Test
    public void shouldMake_Any_Move() {
        System.setIn(new BufferedInputStream(new ByteArrayInputStream("0".getBytes())));
        Scanner scanner = new Scanner(System.in);
        Player player = new Player( "Älex" ,scanner);
        String move = player.askTheMove();
        Assert.assertTrue( move.equals("cooperated") || move.equals("cheated") );
    }

    @Test(expected=IllegalArgumentException.class)
    public void shouldHandle_Invalid_Move() {
        System.setIn(new BufferedInputStream(new ByteArrayInputStream("11".getBytes())));
        Scanner scanner = new Scanner(System.in);
        Player player = new Player( "Älex" ,scanner);
        String move = player.askTheMove();
    }



}
