import java.util.Scanner;

public class Player {
    String name;
    private int score;
    private Scanner scanner;

    public Player(String name, Scanner scanner) {
        this.name = name;
        this.score = 0;
        this.scanner = scanner;
    }

    public int getScore() {
        return this.score;
    }

    public String askTheMove() {
        String input = scanner.next();
        if( input.equals( "0" ) ) {
            return "cooperated";
        }else if(input.equals( "1" )){
            return "cheated";
        }else{
            throw new IllegalArgumentException();
        }



    }
}
