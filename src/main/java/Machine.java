import java.util.Map;

public class Machine {

    int playerOneScore;
    int playerTwoScore;

    public Machine() {
        this.playerOneScore = 0;
        this.playerTwoScore = 0;
    }

    public void calculateScore(String playerOneMove, String playerTwoMove) {
        if(!this.checkIfTheMovesAreValid(playerOneMove) || !this.checkIfTheMovesAreValid(playerTwoMove)){
            throw new IllegalArgumentException();
        }
        this.setPlayersScoreAsPerRule(playerOneMove, playerTwoMove);
    }

    private void setPlayersScoreAsPerRule(String playerOneMove, String playerTwoMove){
        // Assumption: If the player does not cooperate, then he cheats
        if( this.isCooperated( playerOneMove ) ){
            if( this.isCooperated( playerTwoMove ) ) {
                this.playerOneScore = 2;
                this.playerTwoScore = 2;
            } else {
                this.playerOneScore = -1;
                this.playerTwoScore = 3;
            }

        }else {
            if( this.isCooperated( playerTwoMove ) ) {
                this.playerOneScore = 3;
                this.playerTwoScore = -1;
            } else {
                this.playerOneScore = 0;
                this.playerTwoScore = 0;
            }

        }
    }

    private boolean checkIfTheMovesAreValid(String move) {
        if(move==null || (!move.equals("cheated") && !move.equals("cooperated"))){
            return false;
        }
        return true;
    }

    private boolean isCooperated(String move) {
        return move.equals( "cooperated" ) ;
    }

}
